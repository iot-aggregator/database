FROM postgres:13.4-alpine
ENV POSTGRES_DB iotAggregatorDB
COPY iotAggregatorDB.sql /docker-entrypoint-initdb.d/
