CREATE TYPE DEVICE_TYPE AS ENUM ('sensor', 'controller');

CREATE TYPE SENSOR_TYPE AS ENUM ('digital', 'analog');
CREATE TYPE STATUS AS ENUM ('active', 'inactive');

CREATE TABLE raspberries
(
    id          UUID PRIMARY KEY,
    name		VARCHAR NOT NULL,
    tube        INT NOT NULL CHECK ( tube >= 0 )
);

CREATE TABLE devices_on_pins
(
    id                  UUID PRIMARY KEY,
    rpi_id              UUID REFERENCES raspberries(id) ON DELETE CASCADE,
    pin_name            VARCHAR NOT NULL,
    status              STATUS NOT NULL,
    device_type         DEVICE_TYPE NOT NULL,
    device_name         VARCHAR NOT NULL,
    description         VARCHAR,
    measurement_type    VARCHAR,
    sensor_type         SENSOR_TYPE,
    coefficient_a       DOUBLE PRECISION,
    coefficient_b       DOUBLE PRECISION,
    waiting_time_sec    INT,
    action_time_sec     INT,
    CONSTRAINT devices_on_pins_UNIQUE UNIQUE (rpi_id, pin_name),
    CONSTRAINT check_pin_name CHECK ((device_type='sensor' AND pin_name ~'(ANALOG|GPIO)\d+') OR (device_type='controller' AND pin_name ~'GPIO\d+')),
    CONSTRAINT devices_names_on_rpi_UNIQUE UNIQUE (rpi_id, device_name),
    CONSTRAINT hierarchy_null_checks CHECK
        (
        (
            device_type='controller' AND
            measurement_type IS NULL AND
            sensor_type IS NULL AND
            coefficient_a IS NULL AND
            coefficient_b IS NULL AND
            waiting_time_sec >= 0 AND
            action_time_sec >= 0
            )
             OR
        (
            device_type='sensor' AND
            measurement_type IS NOT NULL AND
            sensor_type IS NOT NULL AND
            coefficient_a IS NOT NULL AND
            coefficient_b IS NOT NULL AND
            waiting_time_sec IS NULL AND
            action_time_sec IS NULL
            )
        )
);
